import React, {Component} from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import axios from 'axios';

export default class CreateUser extends Component {

    constructor(props) {
        super (props)
    

    //setting up functions
    this.onChangeUserName = this.onChangeUserName.bind(this);
    this.onChangeUserLastname = this.onChangeUserLastname.bind(this);
    this.onChangeUserPassword = this.onChangeUserPassword.bind(this);
    this.onChangeUserEmail = this.onChangeUserEmail.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    //setting up the state
    this.state = {
        name : '',
        lastName: '',
        password: '',
        email: ''
    }
}

    onChangeUserName(e){
        this.setState({ name: e.target.value})
    }

    onChangeUserLastname(e){
        this.setState({ lastName: e.target.value})
    }

    onChangeUserPassword(e){
        this.setState({ password: e.target.value})
    }

    onChangeUserEmail(e){
        this.setState({ email: e.target.value})
    }

    onSubmit(e){
        e.preventDefault()

        console.log('User successfully created!');
        console.log('Name: ${this.state.name}');
        console.log('LastName: ${this.state.lastName}');
        console.log('Password: ${this.state.password}');
        console.log('Email: ${this.state.email}');
        
        const userObject = {
            name : this.state.name,
            lastName : this.state.lastName,
            password: this.state.password,
            email : this.state.email
        };

        axios.post('http://localhost:5000/users/create-user', userObject)
            .then(res => console.log(res.data));

        this.setState({name: '', lastName: '', password: '', email: ''})
    }




  render() {
    return (<div class="form-wrapper">
      <Form onSubmit={this.onSubmit}>
        <Form.Group controlId="Name">
          <Form.Label>Name</Form.Label>
          <Form.Control type="text" value={this.state.name} onChange={this.onChangeUserName}/>
        </Form.Group>
        <Form.Group controlId="Name">
          <Form.Label>LastName</Form.Label>
          <Form.Control type="text" value={this.state.lastName} onChange={this.onChangeUserLastname}/>
        </Form.Group>
        
        <Form.Group controlId="Name">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" value={this.state.password} onChange={this.onChangeUserPassword}/>
        </Form.Group>

        <Form.Group controlId="Email">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" value={this.state.email} onChange={this.onChangeUserEmail}/>
        </Form.Group>


        <Button variant="danger" size="lg" block="block" type="submit">
          Create User
        </Button>
      </Form>
    </div>);
  }
}