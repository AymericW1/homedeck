import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';
import UserTableRow from './userTableRow';

export default class userList extends Component {
    constructor(props) {
        super(props)
        this.state = {
          users: []
        };
      }
    
      componentDidMount(){
          axios.get('http://localhost:5000/users/')
            .then(res => {
                this.setState({
                    users: res.data
                })
            })
            .catch((error) => {
                console.log(error);
            })
      }

      DataTable(){
          return this.state.users.map((res, i) => {
              return <UserTableRow obj={res} key={i} />;
          });
      }



    render() {
        return (
            <div classname="table-wrapper">
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Lastname</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.DataTable()}
                    </tbody>
                </Table>
                
            </div>
        );
    }
}